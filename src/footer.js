import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  logo: {
    height: "2em"
  },
  footerStyle: {
    width: "100%",
    height: "100px",
    borderTop: "1px solid #eaeaea",
    display: "flex",
    justifyContent: "center",
    aligItems: "center"
  },
  footerLink: {
    color: "inherit",
    textDecoration: "none",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));

export default function Footer(props) {
  const classes = useStyles();
  return (
    <footer className={classes.footerStyle}>
      <a href="/" className={classes.footerLink}>
        <Typography variant="body1">Powered by&nbsp;</Typography>
        <img src="/images/dealtech-logo-full-28.png" alt="DealTech Logo" className={classes.logo} />
      </a>
    </footer>
  );
}
