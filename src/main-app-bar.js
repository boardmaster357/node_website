import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import Hidden from "@material-ui/core/Hidden";

import SearchBar from "./search-bar.js";
import NavTabs from "./nav-tabs.js";

const useStyles = makeStyles(theme => ({
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  },
}));

export default function MainAppBar(props) {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <Toolbar>
        {/*
        //<Hidden mdUp>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton>
        //</Hidden>
        */}
        <Hidden mdDown>
          <Typography className={classes.title} variant="h5" noWrap>
            DealTech
          </Typography>
        </Hidden>
        <SearchBar />
        <Hidden smDown>
          <NavTabs value={props.value} />
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}
