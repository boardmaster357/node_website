import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";

function LinkTab(props) {
  return <Tab component="a" {...props} />;
}

const useStyles = makeStyles(theme => ({
  tab: {
    textTransform: "none",
    fontSize: "1.2rem",
    minWidth: 10
  }
}));

export default function NavTabs(props) {
  //console.log('nav-tabs = ' + props.value);
  const indexLookup = {
    home: 0,
    //about: 1,
    //blog: 2,
    support: 1
  };

  let tab = indexLookup[props.value];
  if (undefined === tab) {
    tab = 0;
  }
  const classes = useStyles();
  const [value, setValue] = React.useState(tab);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Tabs variant="fullWidth" value={value} onChange={handleChange} aria-label="nav tabs example">
      <LinkTab className={classes.tab} label="Home" href="/" />
      {/*<LinkTab className={classes.tab} label="About" href="/about"/>
      <LinkTab className={classes.tab} label="Blog" href="/blog"/>*/}
      <LinkTab className={classes.tab} label="Support" href="/support" />
    </Tabs>
  );
}
