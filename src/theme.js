import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";

// Create a theme instance.
let theme = createMuiTheme({
  typography: {
    h1: {
      fontWeight: 700
    },
    h2: {
      fontWeight: 400
    },
    body1: {
      fontSize: "1.5rem"
    }
  }
});

theme = responsiveFontSizes(theme);

export default theme;
