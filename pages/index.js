import Head from "next/head";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Link from "next/link";
import ReactPlayer from "react-player/youtube";

import MainAppBar from "../src/main-app-bar.js";
import Footer from "../src/footer.js";

const useStyles = makeStyles(theme => ({
  howToBuyALaptop: {
    marginTop: "1rem",
    marginBottom: "1.5rem",
    [theme.breakpoints.down("md")]: {
      marginTop: "0.2rem",
      marginBottom: "0.5rem",
      fontSize: "5.5rem"
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: "0.2rem",
      marginBottom: "0.5rem",
      fontSize: "4rem"
    },
    [theme.breakpoints.down("xs")]: {
      marginTop: "0.2rem",
      marginBottom: "0.5rem",
      fontSize: "2.4rem"
    }
  },
  link: {
    textDecoration: "none",
    color: "#f50057",
    "&:hover": {
      textDecoration: "underline"
    }
  },
  container: {
    minHeight: "100vh",
    padding: "0 0.5rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  main: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  grid: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "960px",
    marginTop: "0.5rem",
    [theme.breakpoints.down("md")]: {
      marginTop: "0.2rem"
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: "0rem"
    }
  },
  playerWrapper: {
    position: "relative",
    paddingTop: "56.25%" /* Player ratio: 100 / (1280 / 720) */,
    width: "100%",
    height: "100%"
  },
  reactPlayer: {
    position: "absolute",
    top: 0,
    left: 0
  }
}));

export default function Home() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <MainAppBar value="home" />
      <div className={classes.container}>
        <Head>
          <title>DealTech</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={classes.main}>
          <Typography className={classes.howToBuyALaptop} variant="h1">
            How to buy a Laptop
          </Typography>
          <div className={classes.playerWrapper}>
            <ReactPlayer url="https://www.youtube.com/watch?v=zHtg-nAhDoE" className={classes.reactPlayer} width="100%" height="100%" />
          </div>
          <div className={classes.grid}>
            <Typography variant="h2">Abstract</Typography>
            <Typography variant="body1">The first step to take before purchasing a laptop is to determine why you need it and features relevant to your requirements. Peculiarities to consider in your analysis include platform, screen size, RAM and ROM, Central processing unit, battery life, connectivity, peripherals, and model and type. This paper recommends using third party reviews to gauge the quality of laptops.</Typography>
            <Typography variant="body1">Determining the right laptop that suits your needs can be a daunting task. Numerous gadgets with different features characterize the contemporary electronics market. While some devices are versatile enough to run demanding applications, others are light hence easy to be carried around. Understanding your needs is critical for identifying the right computer. This paper presents key qualities to consider when shopping for a laptop.</Typography>
            <Typography variant="h2">Identify Needs</Typography>
            <Typography variant="body1">
              Identifyin g why you need a laptop is vital for determining the type of gadget to buy. Light users need simple machines for web-surfing, paying online bills, social networking, responding to Emails, sharing digital photos, and running other light programs. Average users require laptops for streaming and storing music, running programs like Microsoft office, and executing simple video editing programs. People with demanding needs require machines with sophisticated graphics,
              multitasking capabilities, and video creation abilities. Laptops for data entry may require keyboards with separate numeric keypads. Understanding the nature of use is essential for making the right choice. Platform.
            </Typography>
            <Typography variant="body1">
              The platform is the engine of a laptop. It determines the kind of applications you can install and the type of hardware your computer can support. Popular laptop platforms include macOS, Chrome OS, and Windows. macOS is exclusively designed for Mac computers. The platform is easy to use, aesthetic, and secure. But it is relatively expensive. The chrome OS powers Chromebooks. It is inexpensive, very reliable, but quite limited when compared to macOS and Windows. Windows runs on most
              typical laptops. It is the most flexible operating system with an advanced design for touch screen devices. Despite being the most sophisticated operating system, it is quite affordable compared to macOS. Its only weakness is susceptibility to attacks.
            </Typography>
            <Typography variant="h2">Screen</Typography>
            <Typography variant="body1">
              Before examining the specs, it is advisable to figure out how portable you need your laptop to be. The screen size determines the size and portability of laptops. 11 to 12 inch screens are typically thin and light with a weight of 2.5 to 3.5 pounds. 13 to 14 inch provides the best balance for portability and usability, especially if it weighs below 4 pounds. 15 to 16 inch weigh between 4 and 5.5 pounds and are regarded as suitable for people who don't need to move around much often
              with a laptop. 17 to 18 inches are meant for high processing activities like gaming and media production, but only convenient for people with low portability demands. Resolution is also an essential aspect for regular gamers and graphics designers-consider resolutions above Quad HD. Other significant factors to consider are display type and if the laptop should be touch screen.
            </Typography>
            <Typography variant="h2">RAM</Typography>
            <Typography variant="body1">
              The Random Access Memory is essential for enabling the processor to run several programs simultaneously or process vast quantities of data quickly. Typical apps will do fine with a 2GB or 4GB RAM. Advanced users may require RAM of 8GB and above. It is recommended to go for laptops that memory can be expanded. Personal computers are often used for storing information, and sometimes they act as a backup to mobile phones. It is crucial to determine your internal storage requirements.
              250GB or 500GB works best for the majority of people. Youths looking for limitless space to store movies and music can consider 1TB or even 2TB hard drives. Type of internal memory is also mattering; hard disk drives are noisy and heavy but inexpensive, whereas solid-state drives are fast and quiet but costly.
            </Typography>
            <Typography variant="h2">CPU</Typography>
            <Typography variant="body1">
              The processor acts as the brain of a computer. It is responsible for executing all programs and user inputs. The capability of a processor determines the complexity of applications your computer can run. Besides, it dictates the processing speed and the number of programs users can use at the same time. Dual-core processors are suitable for light tasks such as surfing and writing emails. For complex programs, it is recommended to employ advanced processors like core i5 or i7. Core
              x-series are suitable for gaming laptops. Relevant AMD processors also serve diverse processing needs.
            </Typography>
            <Typography variant="h2">Battery</Typography>
            <Typography variant="body1">
              Battery life is determined by a myriad of factors, including some which we have highlighted in this paper. For example processors with high processing power drain batteries quite fast. Hard disk drives and big screens demand significant quantities of power to run. Added accessories such as graphics cards, mouse, and gaming cards increase battery consumption. The type of use should dictate your preference of battery life – laptops designed to be used explicitly on a desk may not
              need long battery life as compared to mobile devices. Good battery life should average at least 7 hours. However, buyers need to understand that long battery life often comes with sacrificing performance.
            </Typography>
            <Typography variant="h2">Connectivity</Typography>
            <Typography variant="body1">
              Computers need to connect to external devices like televisions, phones, cameras, and local networks. Adapters are great for enabling connectivity, but inbuilt avenues of connectivity not only offer convenience but also eliminate the need for extra spending on connectivity tools. A standard laptop should have at least two 3.0 USB ports, network port, HDMI port, media card slots, and a headphone jack. Also, it should provide technologies for wireless networking and standard Bluetooth
              connectivity.
            </Typography>
            <Typography variant="h2">Peripherals</Typography>
            <Typography variant="body1">Peripherals such as a mouse, printer, and external hard drives substantially enhance the usability of laptops. External hard drives provide extra storage to keep movies, music, and programs. Furthermore, it can act as a backup instrument for valuable documents. It is noble to consider peripherals on offer and select a laptop with pertinent accessories that suit your needs.</Typography>
            <Typography variant="h2">Model and Type</Typography>
            <Typography variant="body1">
              Your laptop is only as good as the company that produces it. Robust and timely technical support is paramount for optimal functioning of computers. Leading brands like HP and Dell have websites that offer customer support and resources like drivers and software. It is also vital to consider the laptop type. Instead of buying conventional laptops, gamers can opt for enhanced gaming laptops. Lovers of versatility can buy 2-in-1 laptops. Apart from portability, they are powerful, and
              screen position can be adjusted at 360° to suit user activity.
            </Typography>
            <Typography variant="h2">Conclusion</Typography>
            <Typography variant="body1">There is always a laptop for everyone. What is important is recognizing your needs and determining tradeoffs. Your budget may also determine the product quality you buy. While visiting official producers’ websites may give you realistic price ranges, features are often exaggerated. It is recommended you check reviews from third party websites.</Typography>
            <Typography variant="h4">Sources</Typography>
            <div>
              <Typography variant="body2">
                Ashley Biancuzzo (2019). 7 Things To Consider When Buying A New Laptop. Retrieved From:&nbsp;
                <a className={classes.link} href="https://reviewed.com/laptops/features/7-things-to-consider-when-buying-a-new-laptop">
                  reviewed.com/laptops/features/7-things-to-consider-when-buying-a-new-laptop
                </a>
              </Typography>
              <Typography variant="body2">
                Best Buy. Laptop Buying Guide. Retrieved From:&nbsp;
                <a className={classes.link} href="https://bestbuy.com/site/buying-guides/laptop-buying-guide/pcmcat310900050007.c?id=pcmcat310900050007&intl=nosplash">
                  bestbuy.com/site/buying-guides/laptop-buying-guide/pcmcat310900050007.c?id=pcmcat310900050007&intl=nosplash
                </a>
              </Typography>
              <Typography variant="body2">
                PC World Staff. The top 10 things to consider when buying a new laptop. Retrieved From:&nbsp;
                <a className={classes.link} href="https://pcworld.idg.com.au/article/556585/top-10-things-consider-when-buying-new-laptop">
                  pcworld.idg.com.au/article/556585/top-10-things-consider-when-buying-new-laptop
                </a>
              </Typography>
              <Typography variant="body2">
                Phillip Tracy (2020). Laptop buying guide: 8 essential tips to know before you buy. Retrieved From:&nbsp;
                <a className={classes.link} href="https://laptopmag.com/articles/laptop-buying-guide">
                  laptopmag.com/articles/laptop-buying-guide
                </a>
              </Typography>
            </div>
          </div>
        </main>

        <Footer />
      </div>
    </React.Fragment>
  );
}
