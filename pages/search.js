import Head from "next/head";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useRouter } from 'next/router'

import MainAppBar from "../src/main-app-bar.js";
import Footer from "../src/footer.js";

const useStyles = makeStyles(theme => ({
  title: {
    margin: 0,
    lineHeight: 1.15,
  },
  link: {
    textDecoration: "none",
    color: "#f50057",
    "&:hover": {
      textDecoration: "underline"
    }
  },
  container: {
    minHeight: "100vh",
    padding: "0 0.5rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  main: {
    padding: "5rem 0",
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
}));

export default function Support(props) {
  const classes = useStyles();
  const theme = useTheme();
  const router = useRouter();
  const { query } = router.query

  return (
    <React.Fragment>
      <MainAppBar value="search" />
      <div className={classes.container}>
        <Head>
          <title>DealTech</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={classes.main}>
          <Typography className={classes.title} variant="h4">
            Searching for {query}
          </Typography>
        </main>

        <Footer />

      </div>
    </React.Fragment>
  );
}
