import Head from "next/head";
import Typography from "@material-ui/core/Typography";
import { ThemeProvider } from "@material-ui/core/styles";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Link from "next/link";

import MainAppBar from "../src/main-app-bar.js";
import Footer from "../src/footer.js";

const useStyles = makeStyles(theme => ({
  smartWayToBuy: {
    margin: 0,
    lineHeight: "1.15",
    fontSize: "5rem",
    textAlign: "center"
  }
}));

export default function About() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <MainAppBar value="support" />
      <div className="container">
        <Head>
          <title>DealTech</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Typography className={classes.smartWayToBuy} variant="h1">
            Dealtech is the smart way to buy
          </Typography>
          <Typography variant="h2">
            Search for a laptop{" "}
            <Link href="/">
              <a>here</a>
            </Link>
          </Typography>
        </main>

        <Footer />

        <style jsx>{`
          .container {
            min-height: 100vh;
            padding: 0 0.5rem;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }

          main {
            padding: 5rem 0;
            flex: 1;
            display: flex;
            flex-direction: column;
            align-items: center;
          }

          a {
            text-decoration: none;
          }

          .description {
            line-height: 1.5;
            font-size: 1.5rem;
          }

          .grid {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;

            max-width: 800px;
            margin-top: 3rem;
          }

          @media (max-width: 600px) {
            .grid {
              width: 100%;
              flex-direction: column;
            }
          }
        `}</style>

        <style jsx global>{`
          html,
          body {
            padding: 0;
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
          }

          * {
            box-sizing: border-box;
          }
        `}</style>
      </div>
    </React.Fragment>
  );
}
