import Head from "next/head";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import MainAppBar from "../src/main-app-bar.js";
import Footer from "../src/footer.js";

const useStyles = makeStyles(theme => ({
  link: {
    textDecoration: "none",
    color: "#f50057",
    "&:hover": {
      textDecoration: "underline"
    }
  },
  container: {
    minHeight: "100vh",
    padding: "0 0.5rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  main: {
    padding: "5rem",
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  }
}));

export default function Support() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <MainAppBar value="support" />
      <div className={classes.container}>
        <Head>
          <title>DealTech</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={classes.main}>
          <Typography variant="h4">
            To contact us please send an email to:{" "}
            <a className={classes.link} href="mailto: support@dealtech.com">
              support@dealtech.com
            </a>
          </Typography>
        </main>

        <Footer />
      </div>
    </React.Fragment>
  );
}
